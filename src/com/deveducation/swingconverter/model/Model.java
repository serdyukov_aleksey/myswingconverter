package com.deveducation.swingconverter.model;


import java.util.EnumMap;
import java.util.List;

import static com.deveducation.swingconverter.model.Converter.*;

public class Model {
    private Converter converter = new Converter();
    EnumMap enumMap ;
    Object[] categories;
    private List<Unit> units;

    public Object[] getCategories() {
        return getEnumMap().keySet().toArray();
    }

    public EnumMap getEnumMap() {
        return converter.getAllUnitDefinitions();
    }

    public List<Unit> getUnits(Object unitCategory) {
        return converter.getAvailableUnits((Category) unitCategory);
    }
}
