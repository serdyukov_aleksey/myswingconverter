package com.deveducation.swingconverter.controller;

import com.deveducation.swingconverter.model.Converter;
import com.deveducation.swingconverter.model.Model;
import com.deveducation.swingconverter.view.View;

import java.util.List;

import static com.deveducation.swingconverter.model.Converter.*;
import static java.util.Objects.*;

public class Controller
{
    private Model model = new Model();

    public double calcResult(double value, Category category, String convertFrom, String convertTo) {

        double result;
        List<UnitDefinition> enumArr = (List<UnitDefinition>) this.model.getEnumMap().get(category);
        UnitDefinition udOrig = null;
        UnitDefinition udConv = null;
        for (UnitDefinition unitDefs:enumArr) {
            if (unitDefs.UNIT.getUnitName().equals(convertFrom)){
                udOrig=unitDefs;
                break;
            }
        }
        for (UnitDefinition unitDefs:enumArr) {
            if (unitDefs.UNIT.getUnitName().equals(convertTo)){
                udConv=unitDefs;
                break;
            }
        }
        Converter converter = new Converter(category, udOrig);
        result = converter.convert(value, requireNonNull(udConv));

        return result;
    }

    public static void main(String[] args) {
        new View();
    }
}


