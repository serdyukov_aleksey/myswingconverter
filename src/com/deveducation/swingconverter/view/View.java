package com.deveducation.swingconverter.view;
import com.deveducation.swingconverter.model.Model;
import com.deveducation.swingconverter.model.Unit;
import com.deveducation.swingconverter.controller.Controller;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import static com.deveducation.swingconverter.model.Converter.*;

public class View extends JFrame implements ActionListener {
    private static final int WINDOW_WIDTH = 600;
    private static final int WINDOW_HEIGHT = 100;
    private JFrame view = new JFrame();
    private JPanel converterPanel = new JPanel();
    private JComboBox<String> unitCategory = new JComboBox<>();
    private JComboBox<String> originalUnit = new JComboBox<>();
    private JTextField oUnitValue = new JTextField("0");
    private JTextField conResult  = new JTextField("0");
    private JButton convertButton = new JButton("Convert");
    private JButton resetButton = new JButton("Reset");
    private JComboBox<String> convertedUnit = new JComboBox<>();
    private JButton switchButton = new JButton("\u2191  SWITCH  \u2193");
    private JLabel oText = new JLabel("Convert to --->");


    private Model model = new Model();
    private Controller controller = new Controller();

    public View()
    {
        unitCategory.setModel(new DefaultComboBoxModel(model.getCategories()));
        conResult.setEditable(false);

        convertButton.addActionListener(this);

        switchButton.addActionListener((ActionEvent e) -> {
            try {
                int posO = originalUnit.getSelectedIndex();
                int posC = convertedUnit.getSelectedIndex();
                originalUnit.setSelectedIndex(posC);
                convertedUnit.setSelectedIndex(posO);
                double valO = Double.parseDouble(oUnitValue.getText());
                double valC = Double.parseDouble(conResult.getText());
                oUnitValue.setText(Double.toString(valC));
                conResult.setText(Double.toString(valO));
            } catch (Exception npl) {
                JOptionPane.showMessageDialog(null, "The following exception occured: \n" + npl + "\n Please enter a valid entry into the first field.", "Exception occured.", JOptionPane.ERROR_MESSAGE);
            }
        });

        resetButton.addActionListener ((ActionEvent e)->{
            //addToCombo(init);
            oUnitValue.setText("");
            conResult.setText("");
            unitCategory.setSelectedIndex(0);});

        unitCategory.addActionListener((ActionEvent e)-> {

                originalUnit.setEnabled(true);
                oUnitValue.setEnabled(true);
                switchButton.setEnabled(true);
                convertedUnit.setEnabled(true);
                convertButton.setEnabled(true);
                resetButton.setEnabled(true);
                addToCombo(model.getUnits(unitCategory.getSelectedItem()));
        });

        converterPanel.setBounds(10, 10, WINDOW_WIDTH, WINDOW_HEIGHT-80);
        converterPanel.setLayout(new GridLayout(1, 8, 10, 10));
        converterPanel.add(unitCategory);
        converterPanel.add(originalUnit);
        converterPanel.add(oUnitValue);
        converterPanel.add(oText);
        converterPanel.add(convertedUnit).setEnabled(false);
        converterPanel.add(conResult).setEnabled(false);
        converterPanel.add(convertButton).setEnabled(false);
        converterPanel.add(switchButton).setEnabled(false);
        converterPanel.add(resetButton).setEnabled(false);

        view.setContentPane(converterPanel);
        view.setTitle("Converter Tool");
        view.setSize(WINDOW_WIDTH,WINDOW_HEIGHT);
        view.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        view.setResizable(true);
        view.pack();
        view.setLocationRelativeTo(null);
        view.setVisible(true);

    }

    private void addToCombo(List<Unit> units)
    {
        originalUnit.removeAllItems();
        convertedUnit.removeAllItems();

        for (Unit unit : units)
        {
            originalUnit.addItem(unit.getUnitName());
            convertedUnit.addItem(unit.getUnitName());
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        double result = 0;
        double value = Double.parseDouble(oUnitValue.getText());
        String selectedOrig = (String) originalUnit.getSelectedItem();
        String selectedConvert = (String) convertedUnit.getSelectedItem();
        Category category =(Category) unitCategory.getSelectedItem();
        conResult.setText(Double.toString(controller.calcResult(value,category,selectedOrig,selectedConvert)));
        conResult.setEnabled(true);
    }

//    public void debugWriteToFile(String input, Boolean input2, double doubleInput) throws IOException {
//        BufferedWriter writer = new BufferedWriter(new FileWriter("resources/log.txt", true));
//        writer.append("Combo " + input +" exists (T/F)? " + input2 +" Conversion Rate: "+ doubleInput + "\n");
//        writer.close();
//
//
//    }
}

